import numpy as np
import cv2
import torch
import torch.backends.cudnn as cudnn
from typing import Tuple, List, Union
from rich import print

from data import cfg_mnet, cfg_re50
from layers.functions.prior_box import PriorBox
from models.retinaface import RetinaFace
from utils.nms.py_cpu_nms import py_cpu_nms
from utils.box_utils import decode, decode_landm



def check_keys(model, pretrained_state_dict):
    ckpt_keys = set(pretrained_state_dict.keys())
    model_keys = set(model.state_dict().keys())
    used_pretrained_keys = model_keys & ckpt_keys
    unused_pretrained_keys = ckpt_keys - model_keys
    missing_keys = model_keys - ckpt_keys
    print('Missing keys:{}'.format(len(missing_keys)))
    print('Unused checkpoint keys:{}'.format(len(unused_pretrained_keys)))
    print('Used keys:{}'.format(len(used_pretrained_keys)))
    assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
    return True


def remove_prefix(state_dict, prefix):
    ''' Old style model is stored with all names of parameters sharing common prefix 'module.' '''
    print('remove prefix \'{}\''.format(prefix))
    f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
    return {f(key): value for key, value in state_dict.items()}


def load_model(model, pretrained_path, load_to_cpu):
    print('Loading pretrained model from {}'.format(pretrained_path))
    if load_to_cpu:
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage)
    else:
        device = torch.cuda.current_device()
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage.cuda(device))
    if "state_dict" in pretrained_dict.keys():
        pretrained_dict = remove_prefix(pretrained_dict['state_dict'], 'module.')
    else:
        pretrained_dict = remove_prefix(pretrained_dict, 'module.')
    check_keys(model, pretrained_dict)
    model.load_state_dict(pretrained_dict, strict=False)
    return model


class RetinaFace_inference(object):
    def __init__(self,
                 cfg               :dict  = cfg_re50,
                 trained_model_path:str   = './weights/Resnet50_Final.pth',
                 is_using_cpu      :bool  = True,
                 conf_thres        :float = 0.02,
                 top_k             :int   = 5000,
                 nms_thres         :float = 0.4,
                 keep_top_k        :int   = 750) -> None:
        
        torch.set_grad_enabled(False)
        if torch.cuda.is_available() and not is_using_cpu:
            self.device = torch.device('cuda:0')
        else:
            self.device = torch.device('cpu')
            
        self.cfg = cfg
        self.net = RetinaFace(cfg = self.cfg, phase = 'test')
        self.net = load_model(self.net, trained_model_path, is_using_cpu)
        self.net.to(self.device).eval()
        cudnn.benchmark = True
        
        self.resize = 1
        self.conf_thres = conf_thres
        self.top_k = top_k
        self.nms_thres = nms_thres
        self.keep_top_k = keep_top_k

    def __call__(self, image:cv2.Mat):
        assert len(image.shape) == 3
        img = np.float32(image)
        im_height, im_width, _ = img.shape
        scale = torch.Tensor([im_width, im_height, im_width, im_height], 
                             device=self.device)
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0).to(self.device)
        loc, conf, landms = self.net(img)
        priorbox = PriorBox(self.cfg, image_size = (im_height, im_width))
        prior_data = priorbox.forward().to(self.device).data
        boxes = decode(loc.data.squeeze(0), prior_data, self.cfg['variance'])
        boxes = boxes * scale / self.resize
        boxes = boxes.cpu().numpy()
        
        scores = conf.squeeze(0).data.cpu().numpy()[:, 1]
        landms = decode_landm(landms.data.squeeze(0), prior_data, self.cfg['variance'])
        
        scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2]], device=self.device)

        landms = landms * scale1 / self.resize
        landms = landms.cpu().numpy()
        
        # ignore low scores
        inds = np.where(scores > self.conf_thres)[0]
        boxes = boxes[inds]
        landms = landms[inds]
        scores = scores[inds]
        
        # keep top-K before NMS
        order = scores.argsort()[::-1][:self.top_k]
        boxes = boxes[order]
        landms = landms[order]
        scores = scores[order]
        
        # do NMS
        dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        keep = py_cpu_nms(dets, self.nms_thres)
        # keep = nms(dets, args.nms_threshold,force_cpu=args.cpu)
        dets = dets[keep, :]
        landms = landms[keep]

        # keep top-K faster NMS
        dets = dets[:self.keep_top_k, :]
        landms = landms[:self.keep_top_k, :]

        dets = np.concatenate((dets, landms), axis=1)
        
        return dets
        # for b in dets:
        #     if b[4] < 0.6:                                          # conf of output of face detection
        #         continue
        #     text = "{:.4f}".format(b[4])
        #     b = list(map(int, b))
        #     cv2.rectangle(image, (b[0], b[1]), (b[2], b[3]), (0, 0, 255), 2)
        #     cx = b[0]
        #     cy = b[1] + 12
        #     cv2.putText(image, text, (cx, cy),
        #                 cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255))

        #     # landms
        #     cv2.circle(image, (b[5], b[6]), 1, (0, 0, 255), 4)      # RED left eye
        #     cv2.circle(image, (b[7], b[8]), 1, (0, 255, 255), 4)    # Yellow right eye
        #     cv2.circle(image, (b[9], b[10]), 1, (255, 0, 255), 4)   # tím nose
        #     cv2.circle(image, (b[11], b[12]), 1, (0, 255, 0), 4)    # green left mouth
        #     cv2.circle(image, (b[13], b[14]), 1, (255, 0, 0), 4)    # blue right mouth
    
    
    
def perspective_transform(image:cv2.Mat, rect:Union[np.ndarray, List, Tuple]) -> cv2.Mat:
    (tl, tr, br, bl) = np.float32(rect)

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(round(widthA), round(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(round(heightA), round(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    m = cv2.getPerspectiveTransform(rect, dst)
    return cv2.warpPerspective(image, m, (maxWidth, maxHeight))


def align_face(image:cv2.Mat, dets:np.ndarray) -> List[cv2.Mat]:
    faces = []
    for b in dets:
        landm = np.array(((b[5 ], b[6 ]),
                           (b[7 ], b[8 ]),
                           (b[13], b[14]),
                           (b[11], b[12])))
        faces.append(perspective_transform(image, landm))
        
    return faces