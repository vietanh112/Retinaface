from .align_trans import get_reference_facial_points, warp_and_crop_face
import numpy as np
import cv2

class Align(object):
    def __init__(self, crop_size:int = 112, align_type:str='smilarity') -> None:
        """
            align_type: smilarity | affine | cv2_affine
        """
        self.output_size = (crop_size, crop_size)
        self.scale = crop_size / 112.
        self.reference = get_reference_facial_points(default_square = True) * self.scale
        self.align_type = align_type
    
    def __call__(self, img:cv2.Mat, landm:np.ndarray) -> cv2.Mat:
        return warp_and_crop_face(img, landm, self.reference, self.output_size, self.align_type)
    
    
if __name__ == '__main__':
    dets_path = '/home/agent/Documents/Pytorch_Retinaface/det.npy'
    imgp = '/home/agent/Documents/Pytorch_Retinaface/data/FDDB/images/2002/08/01/big/img_1528.jpg'

    image = cv2.imread(imgp)

    dets = np.load(dets_path)

    facial5points = [
                    [325.0511, 190.60928],      # left eye
                    [365.97107, 183.45375],     # right eye
                    [348.19702, 206.42444],     # nose
                    [335.8785, 233.54347],      # left mouth
                    [370.31284, 228.33748]]     # right mount
    

    # cv2.imshow('feasfae',image)
    # cv2.waitKey(0)