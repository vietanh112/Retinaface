### format output của test_widerface

```
1 0_Parade_marchingband_1_20        // tên ảnh
2 7                                 // số lượng face detect đc
3 542 358 36 42 0.9977931           // từ dòng này đến cuôi file là tọa độ của các box theo format
4 254 369 32 37 0.9968832           // left top wid hei conf
5 30 405 27 31 0.99520195 
6 465 354 27 32 0.9917481 
7 587 368 18 21 0.9798465 
8 853 274 42 50 0.9682538 
9 82 392 21 24 0.957705 


```

### format datatrain `data/widerface/train/label.txt`

```
# relative_path từ folder `data/widerface/train/images` đến file ảnh (.jpg)
449 330 122 149 488.906 373.643 0.0 542.089 376.442 0.0 515.031 412.83 0.0 485.174 425.893 0.0 538.357 431.491 0.0 0.82
là tọa độ face 
```