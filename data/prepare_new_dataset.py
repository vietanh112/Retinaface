import os, posixpath as p
from glob import glob, iglob
from more_itertools import chunked
from rich import print
import cv2


img_dir = 'data/widerface/train/images'
label_file_path = 'data/label.txt'


def get_face_coor(line:str):
    path, coor = line.split(r'.jpg ', 1)
    coor = (round(float(c)) for c in coor.split(' '))
    coor = (tuple(box) for box in chunked(coor, 4))
    return (path.strip(r'# ') + '.jpg', ) + tuple(coor)


def get_coor_face(label_file_path:str = label_file_path):
    read_lbl = open(label_file_path, 'r')
    data = str(next(read_lbl)).strip()

    for i, line in enumerate(read_lbl, 2):
        line = line.strip()
        # print(i, ' -> ', line)
        if line.startswith(r'# '):
            yield get_face_coor(data)
            data = line
        else:
            data += ' ' + line
            
    read_lbl.close()
    yield get_face_coor(data)
    

for data in get_coor_face('data/label.txt'):
    imgp = p.join(img_dir, data[0])
    image = cv2.imread(imgp)
    print('image size ', image.shape)
    for box in data[1:]:
        cv2.circle(image, (box[0], box[1]), 4, (0, 0, 255), 2)
    cv2.imshow('fawf', image)
    cv2.waitKey(0)
    break