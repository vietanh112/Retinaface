import posixpath as p
from glob import iglob, glob
import cv2
from rich import print



imgp = r'0--Parade/0_Parade_Parade_0_904.jpg'
line = r'361 98 263 339 424.143 251.656 0.0 547.134 232.571 0.0 494.121 325.875 0.0 453.83 368.286 0.0 561.978 342.839 0.0 0.89'
'xmin, ymin, wid, hei, x_left_eye, y_left_eye,'
0,     1,    2,   3,   4,         5,          

# 0-3       xmin, ymin, w, h
# 4-5       x, y left eye
# 6         None
# 7-8       x, y right eye
# 10-11     x, y nose
# 12        None
# 13-14     x, y left mouth
# 15        None
# 16-17     x, y right mount
# 18-19     None


imgp = p.join('data/widerface/train/images', imgp)
image = cv2.imread(imgp)


line = line.rstrip().split(' ')
label = [float(x) for x in line]

# bbox
x1, y1 = label[0], label[1]
x2, y2 = label[0] + label[2], label[1] + label[3]
# cv2.rectangle(image, (x1, y1), (x2, y2), (0, 0, 255), 3)

# landmarks
l0 = (label[4], label[5])       # left eye
l1 = (label[7], label[8])       # right eye
l2 = (label[10], label[11])     # nose
l3 = (label[13], label[14])     # left mouth
l4 = (label[16], label[17])     # right mount


# các index khác

cv2.circle(image, center= tuple(map(round, l4)), 
           radius=4, color=(0, 0, 255), thickness=-1)

cv2.imshow('febafe', image)
cv2.waitKey(0)
